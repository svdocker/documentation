FROM sphinxdoc/sphinx:5.3.0

WORKDIR /proj
ADD requirements.txt /proj

ARG DEBIAN_FRONTEND=noninteractive


RUN apt-get update \
    && apt-get -y -qq install make wget ca-certificates fonts-dejavu openjdk-11-jdk graphviz \
    && apt-get clean \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir  -r requirements.txt

RUN wget http://downloads.sourceforge.net/project/plantuml/plantuml.jar -P /opt/ \
    && echo '#!/bin/sh -e\njava -jar /opt/plantuml.jar "$@"' > /usr/local/bin/plantuml \
    && chmod +x /usr/local/bin/plantuml


EXPOSE 8000 35729

